﻿using AutomationPracticeFramework.Common;
using AutomationPracticeFramework.Misc;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Pages
{
    public class Authenticate : HomePage
    {
        public Authenticate()
        {
            CommonMethods.VerifyPageLoad(BtnLogin);
        }

        #region IWebElement
        private IWebElement TxtEmailAddress = Driver.FindElement(By.Id("email"));
        private IWebElement TxtPassword = Driver.FindElement(By.Id("passwd"));
        private IWebElement BtnLogin = Driver.FindElement(By.Id("SubmitLogin"));
        private IWebElement TxtCreateEmail = Driver.FindElement(By.Id("email_create"));
        private IWebElement BtnCreateAccount = Driver.FindElement(By.Id("SubmitCreate"));
        private IWebElement lnkForgotPassword = Driver.FindElement(By.ClassName("lost_password"));       
        #endregion

        #region
        public MyAccount Login(string emailAddress, string password)
        {
            TxtEmailAddress.SendKeys(emailAddress);
            TxtPassword.SendKeys(password);
            BtnLogin.Click();
            return new MyAccount();
        }

        
        //public MyAccount CreateNewAccount(string emailAddress)
        //{
        //    TxtCreateEmail.SendKeys(emailAddress);
        //    BtnCreateAccount.Click();
        //    EditAccountInformation editAccount = new EditAccountInformation(emailAddress);
        //    return editAccount.UpdateInformation();     
        //}        
       

        #endregion

    }
}
