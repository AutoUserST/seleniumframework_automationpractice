﻿using AutomationPracticeFramework.Common;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Pages
{
    public class Tops : Women
    {
        public Tops()
        {
            CommonMethods.VerifyPageLoad(PageHeading, "TOPS");
        }

        #region IWebElement
        private IWebElement PageHeading => Driver.FindElement(By.ClassName("page-heading"));
        #endregion
    }
}
