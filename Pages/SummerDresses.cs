﻿using AutomationPracticeFramework.Common;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Pages
{
    public class SummerDresses : Dresses
    {
        public SummerDresses()
        {
            CommonMethods.VerifyPageLoad(PageHeading, "SUMMER DRESSES");
        }

        #region IWebElement
        private IWebElement PageHeading => Driver.FindElement(By.ClassName("page-heading"));
        #endregion
    }
}
