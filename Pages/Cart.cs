﻿using AutomationPracticeFramework.Common;
using AutomationPracticeFramework.Misc;
using OpenQA.Selenium;

namespace AutomationPracticeFramework.Pages
{
    public class Cart: Product
    {
        public Cart()
        {
            CommonMethods.VerifyPageLoad(BtnCheckout);
        }

        #region IWebElements
        private IWebElement BtnCheckout => Driver.FindElement(By.CssSelector(".standard-checkout[title='Proceed to checkout']"));
        private IWebElement BtnAddressCheckout => Driver.FindElement(By.CssSelector(".btn[name='processAddress']"));
        private IWebElement BtnShippingCheckout => Driver.FindElement(By.CssSelector(".btn[name='processCarrier']"));
        private IWebElement ChkTerms => Driver.FindElement(By.ClassName("checker"));
        private IWebElement LnkPayByCheck => Driver.FindElement(By.ClassName("cheque"));
        private IWebElement LnkPayByBankWire => Driver.FindElement(By.ClassName("bankwire"));
        private IWebElement BtnConfirmOrder => Driver.FindElement(By.CssSelector(".button-medium[type = 'submit']"));
        #endregion

        #region Methods

        protected void Checkout()
        {
            BtnCheckout.Click();
            BtnAddressCheckout.Click();
            ChkTerms.Click();
            BtnShippingCheckout.Click();
        }

        protected void PayByBankWire()
        {
            LnkPayByBankWire.Click();
            BtnConfirmOrder.Click();
        }

        protected void PayByCheck()
        {
            LnkPayByCheck.Click();
            BtnConfirmOrder.Click();
        }

       

        

        #endregion
    }
}