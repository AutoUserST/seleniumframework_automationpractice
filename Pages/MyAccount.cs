﻿using AutomationPracticeFramework.Common;
using OpenQA.Selenium;

namespace AutomationPracticeFramework.Pages
{
    public class MyAccount : BasePage
    {
        public MyAccount()
        {
            CommonMethods.VerifyPageLoad(AccountInfo);
        }

        #region IWebElement
        private IWebElement AccountInfo => Driver.FindElement(By.ClassName("info-account"));
        private IWebElement BtnHomePage => Driver.FindElement(By.CssSelector(".btn[title = 'Home']"));
        private IWebElement LnkHistoryAndDetails => Driver.FindElement(By.CssSelector("a[href*='controller=history'][title='Orders']"));
        private IWebElement LnkCreditSlips => Driver.FindElement(By.CssSelector("a[href*='controller=order-slip'][title='Credit slips']"));
        private IWebElement LnkAddress => Driver.FindElement(By.CssSelector("a[href*='controller=addresses'][title='Addresses']"));
        private IWebElement LnkPersonalInfo => Driver.FindElement(By.CssSelector("a[href*='controller=identity'][title='Information']"));
        private IWebElement LnkWishlist => Driver.FindElement(By.CssSelector("a[href*='controller=mywishlist'][title='My wishlists']"));
        #endregion

        #region Methods
        public BasePage GoBackToHomePage()
        {
            BtnHomePage.Click();
            return new BasePage();
        }


        public void ViewOrderHistory()
        {
            LnkHistoryAndDetails.Click();
        }

        public void ViewCreditSlips()
        {
            LnkCreditSlips.Click();
        }

        public void ViewMyAddress()
        {
            LnkAddress.Click();
        }

        public void ViewMyPersonalInformation()
        {
            LnkPersonalInfo.Click();
        }

        public void ViewMyWishlist()
        {
            LnkWishlist.Click();
        }

        #endregion
    }
}
