﻿using AutomationPracticeFramework.Common;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Pages
{
    public class Dresses : Women
    {
        public Dresses()
        {
            CommonMethods.VerifyPageLoad(PageHeading, "DRESSES");
        }

        #region IWebElement
        private IWebElement PageHeading => Driver.FindElement(By.ClassName("page-heading"));
        #endregion
    }
}
