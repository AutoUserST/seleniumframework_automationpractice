﻿using AutomationPracticeFramework.Common;
using AutomationPracticeFramework.Misc;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Pages
{
    public class Women : Product
    {
        public Women()
        {
            CommonMethods.VerifyPageLoad(PageHeading, "WOMEN");
        }

        #region IWebElement
        private IWebElement PageHeading => Driver.FindElement(By.ClassName("page-heading"));
        private SelectElement SortByValue => new SelectElement(Driver.FindElement(By.Id("selectProductSort")));
        private IWebElement LnkAddToCompare => Driver.FindElement(By.ClassName("add_to_compare"));
        #endregion

        #region Methods
        protected void SortBy(string value = null)
        {
            if (value == null)
                SortByValue.SelectByIndex(0);
            else
                SortByValue.SelectByValue(value);
        }
        
        #endregion

    }
}
