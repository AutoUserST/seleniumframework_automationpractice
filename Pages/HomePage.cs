﻿using AutomationPracticeFramework.Common;
using AutomationPracticeFramework.Misc;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Pages
{
    public class HomePage : BasePage
    {
        public HomePage()
        {
            if (!BtnSearch.Displayed)
                throw new System.Exception("Inccorect page loaded");
        }

        private bool SearchResultsDisplayed => Driver.FindElements(CommonBys.ProductSearchResultBy).Count > 0;

        #region IWebElement           
        private IWebElement ViewCart => Driver.FindElement(CommonBys.BtnViewCartBy);
        private IWebElement TxtSearchItem => Driver.FindElement(CommonBys.TxtSearchBy);
        private IWebElement BtnSearch => Driver.FindElement(CommonBys.BtnSearchBy);
        private By SearchHeadingBy => By.CssSelector(".page-heading");
        #endregion

        #region Methods
        public T GotoPage<T>() where T : new()
        {
            return new PageNavigator().Navigate<T>();
        }

        //public Cart OpenCart()
        //{
        //    ViewCart.Click();
        //    return new Cart();
        //}

        public bool Search(string searchString)
        {
            TxtSearchItem.SendKeys(searchString);
            BtnSearch.Click();
            WaitUntilDisplayed(SearchHeadingBy);
            var count = Driver.FindElements(CommonBys.ProductSearchResultBy).Count;
            return SearchResultsDisplayed;
        }
        #endregion
    }
}
