﻿using AutomationPracticeFramework.Common;
using AutomationPracticeFramework.Misc;
using AutomationPracticeFramework.Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AutomationPracticeFramework.Pages
{
    public class BasePage
    {
        public static IWebDriver Driver;
        public static WebDriverWait webDriverWait;

        public BasePage()
        {
            Driver = DriverFactory.Driver;
            webDriverWait = DriverFactory.WebDriverWait;
        }

        public bool WaitUntilDisplayed(By by)
        {
            return webDriverWait.Until(x => x.FindElement(by).Displayed);
        }

    }
}
