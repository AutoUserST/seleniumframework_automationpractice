﻿namespace AutomationPracticeFramework.Enum
{
    public enum Page
    {
        Authenticate,
        Women,
        WomenTop,
        WomenTshirt,
        Tshirt,
        WomenDresses,
        Dresses,
        DressesCasual,
        DressesEvening,
        DressesSummer,
        WomenDressesCasual,
        WomenDressesEvening,
        WomenDressesSummer,
        WomenBlouses,
    }
}
