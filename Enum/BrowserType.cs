﻿namespace AutomationPracticeFramework.Enum
{
    public enum BrowserType
    {
        /// For Chrome
        Chrome,
        /// For Firefox
        Firefox,
        /// For IE
        IE,
    }
}
