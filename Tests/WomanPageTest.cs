﻿using AutomationPracticeFramework.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Tests
{
    public class WomenPageTest : BaseTest
    {
        [Test]
        
        public void VerifyNavigationToWomenPage(string name)
        {
            Console.WriteLine(name);
            var basePage = IsLandingPageLoadedSucessfully();
            Women womenPage = basePage.GotoPage<Women>();
            Assert.IsTrue((womenPage != null), "Nevigation to Woman page is successful.");
        }
    }
}
