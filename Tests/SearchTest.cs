﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Tests
{
    public class SearchTest : BaseTest
    {
        [Test]        
        public void VerifySearchWithValidInput()
        {
            var homePage = IsLandingPageLoadedSucessfully();
            Assert.IsTrue(homePage.Search("Short"), "Search worked successfully with valid input.");
           
        }

        [Test]
        public void VerifySearchWithInvalidInput()
        {
            var homePage = IsLandingPageLoadedSucessfully();
            Assert.IsFalse(homePage.Search("abc"), "No result displayed for invalid input.");
        }

        [Test]
        [TestCase ("Short", "True")]
        [TestCase ("abc", "False")]
        public void VerifySearch(string searchString, string expectedResult)
        {
            var homePage = IsLandingPageLoadedSucessfully();
            Assert.AreEqual(homePage.Search(searchString).ToString(), expectedResult, "Search worked successfully");

        }
    }
}
