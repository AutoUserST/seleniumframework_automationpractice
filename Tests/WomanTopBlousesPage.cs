﻿using AutomationPracticeFramework.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Tests
{
    public class WomanTopBlousesPage : BaseTest
    {
        [Test]
        public void VerifyNavigationToBlousesPage()
        {
            var basePage = IsLandingPageLoadedSucessfully();
            WomenBlouses blousesPage = basePage.GotoPage<WomenBlouses>();
            Assert.IsTrue((blousesPage != null), "Nevigation to Woman > Top > Blouses page is successful.");
        }
    }
}
