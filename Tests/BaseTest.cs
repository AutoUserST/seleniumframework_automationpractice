﻿using AutomationPracticeFramework.Pages;
using AutomationPracticeFramework.Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AutomationPracticeFramework.Tests
{
    //[TestFixtureSource(typeof(DriverFactory), nameof(DriverFactory.GetBrowsers))]
    public abstract class BaseTest
    {
        public static IWebDriver Driver;
        public static WebDriverWait webDriverWait;

        public BaseTest()
        {
            Driver = DriverFactory.Driver;
            webDriverWait = DriverFactory.WebDriverWait;
        }

        public HomePage IsLandingPageLoadedSucessfully()
        {
            return new HomePage();
        }

        
    }
}