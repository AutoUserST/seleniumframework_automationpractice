﻿using AutomationPracticeFramework.Pages;
using NUnit.Framework;

namespace AutomationPracticeFramework.Tests
{
    public class MyAccountTest : BaseTest
    {
        [Test]
        public void VerifyLoginSuccess()
        {
            var basePage = IsLandingPageLoadedSucessfully();
            var authenticatePage = basePage.GotoPage<Authenticate>();
            var myAccountPage = authenticatePage.Login("one@abc.com", "Test123");
            Assert.IsTrue(!(myAccountPage is null), "User is able to login successfully.");
        }

        //[Test]
        //public void VerifyInvalidUserLogin()
        //{
        //    var basePage = IsLandingPageLoadedSucessfully();
        //    var authenticatePage = basePage.GotoPage<Authenticate>();
        //    var myAccountPage = authenticatePage.Login("one@abc.com", "Test1");
        //    Assert.IsTrue((myAccountPage is null), "User is able to login successfully.");
        //}
    }
}
