﻿using System;

namespace AutomationPracticeFramework.Utility
{
    public abstract class ConfigUtil
    {
        public static string Browser { get; set; }
        public static double DefaultWait { get; set; }       

        static ConfigUtil()
        {
            Browser = ReadConfig.ReadValueForKey("Browser");
            DefaultWait = Convert.ToDouble(ReadConfig.ReadValueForKey("Wait"));
        }       

        //protected static void SetConfigNameValue()
        //{           
        //    foreach(string key in ConfigKeys)
        //    {
        //        ConfigKeyValues.Add(key, ReadConfig.ReadValueForKey(key));
        //    }           
        //}

        //protected static string GetConfigNameValue(string key)
        //{
        //    try
        //    {
        //        SetConfigNameValue();
        //        return ConfigKeyValues.Get(key);
        //    }
        //    catch(ConfigurationException)
        //    {
        //        return null;
        //    }           

        //}  

        //public static double WaitInSeconds => Convert.ToDouble(GetConfigNameValue("wait"));

        //public static List<BrowserType> EnabledBrowsers => GetConfigNameValue("EnabledBrowser").Split(',')
        //    .Select(x => (BrowserType)System.Enum.Parse(typeof(BrowserType), x)).ToList();
    }
}
