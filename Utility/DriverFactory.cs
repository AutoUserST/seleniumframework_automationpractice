﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace AutomationPracticeFramework.Utility
{
    public static class DriverFactory
    {
        public static IWebDriver Driver { get; set; }

        public static string Browsers = ConfigUtil.Browser;

        public static WebDriverWait WebDriverWait { get; set; }

        static DriverFactory()
        {
            switch (Browsers)
            {
                case "Chrome":
                    Driver = new ChromeDriver();
                    break;

                case "Firefox":
                    break;

                case "IE":
                    break;

                default:
                    throw new Exception("Invalid browser");
            }

            Driver.Manage().Window.Maximize();
            WebDriverWait = new WebDriverWait(Driver, TimeSpan.FromMilliseconds(ConfigUtil.DefaultWait));
            Driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
        }
    }
}
