﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Common
{
    public class CommonMethods
    {
       public static bool VerifyPageLoad(IWebElement webElement, string value=null)
       {
            if (value == null)
                return webElement.Displayed;
            else
                return webElement.Text == value;
       }
      
    }
}
