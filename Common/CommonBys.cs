﻿using OpenQA.Selenium;

namespace AutomationPracticeFramework.Common
{
    public class CommonBys
    {       
        public static readonly By BtnSignOutBy = By.ClassName("logout");
        public static readonly By BtnViewCartBy = By.ClassName("shopping_cart");
        public static readonly By TxtSearchBy = By.Id("search_query_top");
        public static readonly By BtnSearchBy = By.CssSelector(".button-search");
        public static readonly By ProductSearchResultBy = By.CssSelector(".product-container");
        
    }
}
