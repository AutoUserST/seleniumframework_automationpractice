﻿using AutomationPracticeFramework.Common;
using AutomationPracticeFramework.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPracticeFramework.Misc
{
    public class Product : HomePage
    {
        Actions builder = new Actions(Driver);
        private string ItemName => ProductName.Text;       
        
        #region IWebElement
        private IWebElement HoverItem => Driver.FindElement(By.ClassName("product-image-container"));
        private IWebElement ViewItem => Driver.FindElement(By.CssSelector(".button.lnk_view"));
        private IWebElement BtnAddToCart => Driver.FindElement(By.ClassName("ajax_add_to_cart_button"));
        private IWebElement BtnContinueShopping => Driver.FindElement(By.CssSelector(".btn[title='Continue shopping']"));
        private IWebElement BtnProceedToCheckout => Driver.FindElement(By.CssSelector(".btn[title = 'Proceed to checkout']"));
        private IWebElement BtnViewAddToCart => Driver.FindElement(By.Id("add_to_cart"));
        private IWebElement ProductName => Driver.FindElement(By.CssSelector(".tab-content #homefeatured .product-container[itemscope] .right-block .product-name"));
        private ReadOnlyCollection<IWebElement> ProductList => Driver.FindElements(By.CssSelector(".tab-content #homefeatured .product-container[itemscope] .right-block .product-name"));
        #endregion

        #region Methods
        public void ViewProduct()
        {
            builder.MoveToElement(HoverItem).Click().Build().Perform();
            ViewItem.Click();
        }

        public Cart AddProductToCartAndContinueShopping()
        {
            BtnAddToCart.Click();
            BtnContinueShopping.Click();
            return new Cart();
        }

        public void AddProductToCartAndProceedToCheckout()
        {
            BtnAddToCart.Click();
            BtnProceedToCheckout.Click();
        }

        public void ViewAndAddProductToCart()
        {
            BtnViewAddToCart.Click();
        }       
       

        public IWebElement SelectItem(string itemName = null)
        {
            if (itemName == null)
                return ProductList.FirstOrDefault();
            else
            {
                foreach(IWebElement element in ProductList)
                {
                    if (element.Text == ItemName)
                        return element;
                }
            }
            
            return null;
        }

        #endregion
    }
}
