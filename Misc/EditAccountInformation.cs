﻿using AutomationPracticeFramework.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace AutomationPracticeFramework.Misc
{
    public class EditAccountInformation : Authenticate
    {
        public EditAccountInformation(string emailAddress)
        {
            AddDefaults(emailAddress);
        }

        #region Properties  
        private string Title { get; set; }
        private string FirstName { get; set; }
        private string LastName { get; set; }
        private string EmailAddress { get; set; }
        private string Password { get; set; }
        private DateTime BirthDate { get; set; }
        private string AddressLine { get; set; }
        private string State { get; set; }
        private string ZipCode { get; set; }
        private string City { get; set; }
        private string Phone { get; set; }

        #endregion

        #region IWebElement
        private IWebElement ChkGenderMale = Driver.FindElement(By.Id("uniform-id_gender1"));
        private IWebElement ChkGenderFemale = Driver.FindElement(By.Id("uniform-id_gender2"));
        private IWebElement TxtFirstName = Driver.FindElement(By.Id("customer_firstname"));
        private IWebElement TxtLastName = Driver.FindElement(By.Id("customer_lastname"));
        private IWebElement TxtEmail = Driver.FindElement(By.Id("email"));
        private IWebElement TxtPassword = Driver.FindElement(By.Id("passwd"));
        private IWebElement TxtAddress = Driver.FindElement(By.Id("customer_lastname"));
        private IWebElement TxtAddrFirstName = Driver.FindElement(By.Id("firstname"));
        private IWebElement TxtAddrLastName = Driver.FindElement(By.Id("lastname"));
        private IWebElement TxtCity = Driver.FindElement(By.Id("city"));
        private SelectElement DrpState = new SelectElement(Driver.FindElement(By.Id("id_state")));
        private IWebElement TxtMobPhone = Driver.FindElement(By.Id("phone_mobile"));
        private IWebElement TxtZipcode = Driver.FindElement(By.Id("postcode"));
        private IWebElement BtnRegisterAccount = Driver.FindElement(By.Id("submitAccount"));
        #endregion

        private void AddDefaults(string emailAddress)
        {
            string guid = Guid.NewGuid().ToString("N").Substring(0, 4);
            FirstName = "AutoFN" + guid;
            LastName = "AutoLN" + guid;
            BirthDate = DateTime.Now.AddYears(-20);            
            Title = "Mrs.";
            AddressLine = $"AutoAddress" + guid;            
            Phone = "1111111111";
            ZipCode = "66502";
            State = "Hawaii";
            this.EmailAddress = emailAddress;
            Password = "auto_123";
        }

        public MyAccount UpdateInformation()
        {
            if (Title == "Mr")
                ChkGenderMale.Click();
            else
                ChkGenderFemale.Click();
            TxtFirstName.SendKeys(FirstName);
            TxtLastName.SendKeys(LastName);
            if (TxtEmail.Text == "")
                TxtEmail.SendKeys(EmailAddress);
            TxtPassword.SendKeys(Password);
            if (TxtAddrFirstName.Text == "")
                TxtAddrFirstName.SendKeys(FirstName);
            if (TxtAddrLastName.Text == "")
                TxtAddrLastName.SendKeys(LastName);
            TxtAddress.SendKeys(AddressLine);
            TxtCity.SendKeys(City);
            DrpState.SelectByValue(State);
            TxtZipcode.SendKeys(ZipCode);

            BtnRegisterAccount.Click();
            return new MyAccount();
        }
       
    }
}
