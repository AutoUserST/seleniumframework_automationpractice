﻿using AutomationPracticeFramework.Enum;
using AutomationPracticeFramework.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Linq;

namespace AutomationPracticeFramework.Misc
{
    public class PageNavigator : HomePage
    {
        Actions builder = new Actions(Driver);

        #region IWebElement
        private IWebElement BtnSignIn => Driver.FindElement(By.ClassName("login"));
        private IWebElement LnkWomen => Driver.FindElement(By.CssSelector(".sf-with-ul[title='Women']"));
        private IWebElement LnkDresses => Driver.FindElement(By.CssSelector(".sf-with-ul[title='Dresses']"));
        private IWebElement LnkTshirts => Driver.FindElement(By.CssSelector(".sfHoverForce [title='T-shirts']"));
        private IWebElement LnkTops => Driver.FindElement(By.CssSelector(".sfHoverForce [title='Tops']"));
        private IWebElement BtnTops => Driver.FindElement(By.CssSelector("#categories_block_left .block_content ul li"));
        private IWebElement LnkBlouses => Driver.FindElement(By.CssSelector(".sf-menu [title='Blouses']"));
        private IWebElement BtnBlouses => Driver.FindElement(By.CssSelector("#categories_block_left .block_content ul li[class='last']"));
        private IWebElement LnkCasualDresses => Driver.FindElement(By.CssSelector(".sf-menu [title='Casual Dresses']"));
        private IWebElement LnkEveningDresses => Driver.FindElement(By.CssSelector(".sf-menu [title='Evening Dresses']"));
        private IWebElement LnkSummerDresses => Driver.FindElement(By.CssSelector(".sf-menu [title='Summer Dresses']"));
        #endregion


        public T Navigate<T>() where T : new()
        {
            string pageAsString = string.Empty;          

            try
            {
                pageAsString = typeof(T).ToString().Split('.').Last();
                System.Enum.TryParse(pageAsString, out Page page);

                switch (page)
                {
                    case Page.Authenticate:
                        BtnSignIn.Click();
                        break;
                    case Page.Dresses:
                        LnkDresses.Click();
                        break;
                    case Page.DressesCasual:
                        builder.MoveToElement(LnkDresses).Click().Build().Perform();
                        LnkCasualDresses.Click();
                        break;
                    case Page.DressesEvening:
                        builder.MoveToElement(LnkDresses).Click().Build().Perform();
                        LnkEveningDresses.Click();
                        break;
                    case Page.DressesSummer:
                        builder.MoveToElement(LnkDresses).Click().Build().Perform();
                        LnkSummerDresses.Click();
                        break;
                    case Page.Women:
                        LnkWomen.Click();
                        break;
                    case Page.WomenBlouses:
                        builder.MoveToElement(LnkWomen).Build().Perform();
                        LnkBlouses.Click();
                        //LnkWomen.Click();
                        //BtnTops.Click();
                        //BtnBlouses.Click();
                        break;
                    case Page.WomenDresses:
                        builder.MoveToElement(LnkWomen).Click().Build().Perform();
                        LnkDresses.Click();
                        break;
                    case Page.WomenDressesCasual:
                        builder.MoveToElement(LnkWomen).Click().Build().Perform();
                        LnkCasualDresses.Click();
                        break;
                    case Page.WomenDressesEvening:
                        builder.MoveToElement(LnkWomen).Click().Build().Perform();
                        LnkEveningDresses.Click();
                        break;
                    case Page.WomenDressesSummer:
                        builder.MoveToElement(LnkWomen).Click().Build().Perform();
                        LnkSummerDresses.Click();
                        break;
                    case Page.WomenTop:
                        builder.MoveToElement(LnkWomen).Click().Build().Perform();
                        LnkTops.Click();
                        break;
                    case Page.WomenTshirt:
                        builder.MoveToElement(LnkWomen).Click().Build().Perform();
                        LnkTshirts.Click();
                        break;
                    case Page.Tshirt:
                        LnkTshirts.Click();
                        break;
                    default:
                        break;
                }
                return new T();
            }
            catch (Exception exception)
            {
                throw new Exception($"While trying to navigate to the page: {pageAsString}, there was an exception thrown: {exception.Message}");
            }
        }
    }
}
